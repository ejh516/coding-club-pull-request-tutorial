sub add {
=pod
=head1 add
    Adds two numbers together
=cut
    my ($x, $y) = @_;
    return $x + $y;
}

sub subtract {
=pod
=head1 subtract
    subtracts two numbers
=cut
    my ($x, $y) = @_;
    return $x - $y;
}

1;
