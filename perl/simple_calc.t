use Test::Simple tests => 8;
use lib "./";
use simple_calc;

ok( add( 2, 2) ==  4 );
ok( add(-2, 2) ==  0 );
ok( add( 2,-2) ==  0 );
ok( add(-2,-2) == -4 );

ok( subtract( 2, 2) ==  0 );
ok( subtract(-2, 2) == -4 );
ok( subtract( 2,-2) ==  4 );
ok( subtract(-2,-2) ==  0 );
